import { createAction, props } from '@ngrx/store';
import { Pessoa } from '../../shared/domain/pessoa';

export enum PessoaActionTypes {
  PESSOA_ALL = '[PESSOA_ALL] Buscar todas as pessoas',
  PESSOA_NEW = '[PESSOA_NEW] Cadastrar uma nova pessoa',
  PESSOA_UPDATE = '[PESSOA_UPDATE] Atualizar uma pessoa',
  PESSOA_DELETE = '[PESSOA_DELETE] Deletar uma pessoa'
}

export const PessoaAll = createAction(PessoaActionTypes.PESSOA_ALL);

export const PessoaNew = createAction(PessoaActionTypes.PESSOA_NEW, props<{pessoa: Pessoa}>());

export const PessoaUpdate = createAction(PessoaActionTypes.PESSOA_UPDATE, props<{pessoa: Pessoa}>());

export const PessoaDelete = createAction(PessoaActionTypes.PESSOA_UPDATE, props<{codigo: number}>());

export type PessoaActions = PessoaAll | PessoaNew | PessoaUpdate | PessoaDelete;
