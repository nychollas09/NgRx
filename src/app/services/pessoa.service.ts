import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as faker from 'faker';
import { Subject } from 'rxjs';
import { Endereco } from '../shared/domain/embedabble/endereco';
import { Pessoa } from '../shared/domain/pessoa';
import { AppState } from '../store/app/app-state';
import { PessoaNew } from '../store/pessoa/pessoa.action';

@Injectable({
  providedIn: 'root'
})
export class PessoaService {
  constructor(private store: Store<AppState>) {}

  pessoaAdicionada$: Subject<Pessoa> = new Subject<Pessoa>();
  pessoaAtualizada$: Subject<Pessoa> = new Subject<Pessoa>();
  pessoaDeletada$: Subject<Pessoa> = new Subject<Pessoa>();

  public adicionarPessoa() {
    this.store.dispatch(new PessoaNew({ pessoa: this._gerarPessoa() }));
  }

  private _gerarPessoa(): Pessoa {
    return {
      codigo: new Date().getMilliseconds(),
      nome: faker.name.findName(),
      idade: Math.round(Math.random() * 100),
      endereco: this._gerarEndereco()
    };
  }

  private _gerarEndereco(): Endereco {
    return new Endereco({
      logradouro: faker.address.streetAddress(),
      cidade: faker.address.city(),
      estado: faker.address.country()
    });
  }
}
